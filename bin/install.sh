#!/bin/sh

# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

#install
rm -rf /otp/services/appstream
mkdir -p /opt/services/appstream
cp -r ./* /opt/services/appstream
rm /otp/services/appstream/bin/install.sh

#add to autostart
cp bin/appstream.sh /etc/init.d/appstream
chmod +x /etc/init.d/appstream
update-rc.d appstream defaults


