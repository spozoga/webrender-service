"use strict";

import {Renderer} from './renderer';
import {Result} from './result';
import {logger} from './logger';

export class PngRenderer extends Renderer {

    render(url) {
        var self = this;
        return self.renderBase(url, self.getPng.bind(self)).then((file)=> {
            let result = new Result(self.getKeyFromUrl(url));
            result.setFile(file);
            return result;
        });
    }

    getPng(page) {
        return this.renderfile(page, 'png');
    }

    getType() {
        return 'png';
    }

}