"use strict";

import {logger} from './logger';

export class Result {

    constructor(key) {
        this.key = key;
        this.data = null;
        this.file = null;
    }

    setData(data) {
        if (this.data || this.file) {
            logger.critic("set result only once");
        }
        this.data = data;
    }

    setFile(file) {
        if (this.data || this.file) {
            logger.critic("set result only once");
        }
        this.file = file;
    }

    saveToCache(cache) {
        var newfile;
        if (this.data) {
            cache.setData(this.key, this.data);
        } else if (this.file) {
            newfile = cache.setFile(this.key, this.file);
            this.file = newfile || this.file;
        } else {
            logger.critic("must set data or file to add it to cache");
        }
    }

    send(res) {
        if (this.data != null) {
            res.send(this.data);
        } else if (this.file != null) {
            res.sendfile(this.file);
        } else {
            logger.critic("must set data or file to send it", [this.data, this.file]);
        }
    }

}