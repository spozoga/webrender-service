"use strict";

class Logger {

    constructor() {
        console.log("----------------------");
        console.log("----- Start app ------");
        console.log("----------------------");
    }

    log(msg, args) {
        var i, out = [], argstr = "", str;
        if (args) {
            for (i = 0; i < args.length; i++) {
                out.push(JSON.stringify(args[i]));
            }
            argstr = " " + out.join(", ");
        }
        str = " *[" + new Date() + "] " + msg + argstr;
        console.log(str);
        return str;
    }

    critic(msg, args) {
        throw this.log(" CRITIC: " + msg, args);
    }

    error(msg, args) {
        return this.log(" ERRROR: " + msg, args);
    }
}

export var logger = new Logger();



