"use strict";

import {Renderer} from './renderer';
import {Result} from './result';
import {logger} from './logger';

export class PdfRenderer extends Renderer {

    render(url) {
        var self = this;
        return self.renderBase(url, self.getPdf.bind(self)).then((file)=> {
            let result = new Result(self.getKeyFromUrl(url));
            result.setFile(file);
            return result;
        });
    }

    getPdf(page) {
        return this.renderfile(page, 'pdf');
    }

    getType() {
        return 'pdf';
    }

}