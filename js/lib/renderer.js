"use strict";

import {ObjectBase} from './object';
import {logger} from './logger';

const CONFIG_KEY = Symbol(),
    PHANTOM_HTML_PARAMETERS = {
        'load-images': 'no',
        'local-to-remote-url-access': 'no'
    },
    PHANTOM_RENDER_PARAMETERS = {
        'load-images': 'yes',
        'local-to-remote-url-access': 'no'
    },
    PHANTOM_300DPI_VIEWPORT = {
        width: 2480,
        height: 3508
    };

var phantom = require('phantom');

export class Renderer extends ObjectBase {

    render() {
        logger.critic("no implement render in abstract class");
    }

    getType() {
        logger.critic("no implement getType in abstract class");
    }

    getKeyFromUrl(url) {
        return this.getType() + '|' + url;
    }

    renderBase(url, cb) {
        return this.renderCB(url, PHANTOM_RENDER_PARAMETERS, PHANTOM_300DPI_VIEWPORT, cb);
    }

    renderCB(url, phantomParameters, viewportParameters, rendercb) {
        let phantomInstance = null,
            close = ()=> {
                if (!phantomInstance) return;
                phantomInstance.exit();
                phantomInstance = null;
                logger.log('renderer.renderCB(' + url + '): close');
            },
            buffer = null;

        return this.createPhantom(phantomParameters).then((ph) => {
            logger.log('renderer.renderCB(' + url + '): create phantom object');
            phantomInstance = ph;
            return this.createPage(ph, url, viewportParameters);
        }).then((page) => {
            logger.log('renderer.renderCB(' + url + '): create page');
            return page;
        }).then((page) => {
            //set timeout
            return new Promise((resolve, reject)=>{
              setTimeout(resolve.bind(this, page), 3000);
            });
        }).then(rendercb).then((result)=> {
            logger.log('renderer.renderCB(' + url + '): get result');
            buffer = result;
        }).then(close, close).then(()=> {
            logger.log('renderer.renderCB(' + url + '): success');
            return buffer;
        }, ()=> {
            logger.log('renderer.renderCB(' + url + '): fail');
            return buffer;
        });
    }

    createPhantom(parameters) {
        let executor = (resolve, reject) => {
            phantom.create({
                parameters: parameters
            }, (ph) => {
                if (!ph) {
                    logger.error("renderer: no create phantom object");
                    reject();
                }
                resolve(ph);
            })
        };
        return new Promise(executor);
    }

    createPage(ph, url, viewPort) {
        let executor = (resolve, reject) => {
            ph.createPage((page) => {
                page.set('viewportSize', viewPort);

                page.open(url, (status) => {
                    if (!status) {
                        logger.log("renderer: page create status fail");
                        reject();
                    }
                    resolve(page, ph);
                });
            });
        };
        return new Promise(executor);
    }

    renderfile(page, type) {
        let filePath = './tmp/' + this.createHash() + '.' + type,
            executor = (resolve, reject) => {
                logger.log('renderer.renderfile(' + filePath + '): render');
                page.render(filePath, ()=> {
                    logger.log('renderer.renderfile(' + filePath + '): finish');
                    resolve(filePath);
                });
            };
        return new Promise(executor);
    }

}
