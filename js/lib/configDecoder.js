"use strict";
import {logger} from './logger';

export class ConfigDecoder {

    constructor(config) {
        this.config = config;
    }

    getTokenRow(token) {
        let tokens = this.config.tokens;
        for (var row of tokens) {
            if (row.token == token) {
                return row;
            }
        }
        logger.error("No find token");
        return false;
    }

    tokenHasRole(token, role) {
        var row = this.getTokenRow(token);
        if (!row) return false;
        for (var rowRole in row.roles) {
            if (rowRole == role) return true;
        }
        return false;
    }

    getProperty(name, defaultValue = false) {
        return this.config[name] != undefined ? this.config[name] : defaultValue;
    }

    isEntitleUrl(url) {
        if (!this.config.allowUrls) return true;
        for (var prefix of this.config.allowUrls) {
            if (url.startsWith(prefix)) return true;
        }
        return false;
    }

    isTokenRole(token, role) {
        var tokenRow = this.getTokenRow(token);
        if (!tokenRow.roles) return true;
        for (var r of tokenRow.roles) {
            if (r == role) return true;
        }
        return false;
    }

    isDebug() {
        return this.config === true;
    }
}