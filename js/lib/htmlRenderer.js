"use strict";

import {Renderer} from './renderer';
import {Result} from './result';
import {logger} from './logger';

export class HtmlRenderer extends Renderer {

    render(url) {
        var self = this;
        return self.renderBase(url, self.getHtml.bind(self)).then((html)=> {
            let result = new Result(self.getKeyFromUrl(url));
            result.setData(html);
            return result;
        });
    }

    getHtml(page) {
        logger.log("renderer.getHtml: start");
        let executor = (resolve, reject) => {
            page.evaluate(function () {
                return document.documentElement.innerHTML;
            }, function (htmlCode) {
                if (!htmlCode) {
                    logger.error("renderer.getHtml: no html code");
                    reject();
                }
                logger.log("renderer.getHtml: success");
                resolve(htmlCode);
            });
        };
        return new Promise(executor);
    }

    getType() {
        return 'html';
    }

}