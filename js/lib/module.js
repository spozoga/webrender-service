"use strict";
import {ObjectBase} from './object';

const DEPS = Symbol(),
    APP = Symbol();

export class Module extends ObjectBase {

    constructor() {
        super();
        this.setPriv(DEPS, {});
    }

    init(deps) {
        this.setPriv(DEPS, deps);
    }

    start() {

    }

    get(name) {
        return this.getPriv(DEPS)[name];
    }
}