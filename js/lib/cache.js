"use strict";
import {ObjectBase} from './object';
import {processApi} from './process';

const BASEPATH_KEY = Symbol(),
    COLLECTION_KEY = Symbol(),
    INDEX_FILE = 'index.json';

var fs = require('fs'),
    path = require('path');

export class Cache extends ObjectBase {

    constructor(basepath = "./data/", cacheConfig = {}) {
        super();
        if (!fs.existsSync(basepath)) {
            fs.mkdirSync(basepath);
        }
        this.setPriv(BASEPATH_KEY, basepath);

        this.cacheConfig = {
            lifetime: cacheConfig.lifetime || 3600000,
            refreshTime: cacheConfig.refreshTime || 36000,
            limit: cacheConfig.limit || 1000
        };

        let dataPath = this.getPath(INDEX_FILE);
        this.setPriv(COLLECTION_KEY, fs.existsSync(dataPath) ? JSON.parse(fs.readFileSync(dataPath)) : {});
        this.modified = false;

        //flush loop
        setInterval(this.flushLoop.bind(this), 1000);

        //expire loop
        setInterval(this.cleanExpired.bind(this), cacheConfig.refreshTime);

        processApi.bindExit(this.flush.bind(this));
    }

    cleanExpired(lifetime) {
        var expiredPoint = new Date().getTime() - lifetime,
            collection = this.getPriv(COLLECTION_KEY),
            modified = false,
            i;

        for (i in collection) {
            if (collection[i].createTime < expiredPoint) {
                fs.unlink(this.getPath(collection[i].hash));
                collection[i] = undefined;
                delete collection[i];
                modified = true;
            }
        }
        this.modified = this.modified || modified;
    }

    clean(base = "") {
        let i, collection = this.getPriv(COLLECTION_KEY), modified = false;

        for (i in collection) {
            if (i.startsWith(base)) {
                fs.unlink(this.getPath(collection[i].hash));
                collection[i] = undefined;
                delete collection[i];
                modified = true;
            }
        }
        this.modified = this.modified || modified;
    }

    get(key) {
        var row = this.getPriv(COLLECTION_KEY)[key], path;
        if (row == null) return null;
        path = this.getPath(row.hash);
        return fs.readFileSync(path);
    }

    getFile(key) {
        var row = this.getPriv(COLLECTION_KEY)[key];
        if (row == null) return null;
        return this.getPath(row.hash);
    }

    setData(url, data, type = 'html') {
        var hash = this.createHash() + '.' + type,
            file = this.getPath(hash),
            collection = this.getPriv(COLLECTION_KEY);

        if (Object.keys(collection).length >= this.cacheConfig.limit) return;

        fs.writeFileSync(file, data);

        collection[url] = {
            createTime: new Date().getTime(),
            hash: hash
        };

        this.modified = true;
    }

    setFile(url, inpath) {
        var filename = path.basename(inpath),
            outpath = this.getPath(filename),
            collection = this.getPriv(COLLECTION_KEY);

        if (Object.keys(collection).length >= this.cacheConfig.limit) return;

        fs.rename(inpath, outpath);

        collection[url] = {
            createTime: new Date().getTime(),
            hash: filename
        };

        this.modified = true;
        return outpath;
    }

    getPath(name) {
        return this.getPriv(BASEPATH_KEY) + name;
    }

    flushLoop() {
        if (this.modified) this.flush();
        this.modified = false;
    }

    flush() {
        var data = this.getPriv(COLLECTION_KEY),
            path = this.getPath(INDEX_FILE);
        fs.writeFile(path, JSON.stringify(data));
    }
}