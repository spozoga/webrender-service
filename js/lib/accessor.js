"use strict";

import {Result} from './result';
import {logger} from './logger';

export class Accessor {

    constructor(renderer, cache = null) {
        this.renderer = renderer;
        this.cache = cache;
    }

    get(url) {
        var key = this.renderer.getKeyFromUrl(url),
            file = this.cache ? this.cache.getFile(key) : null,
            self = this;

        if (file) {
            logger.log('accessor.get(' + key + ') get from cache');
            return this.createResponse(key, file);
        }

        return this.renderer.render(url).then((result)=> {
            logger.log('accessor.get(' + key + ') get from renderer', [result]);
            if (!self.cache) return result;
            result.saveToCache(self.cache);
            return result;
        }, ()=> {
            logger.error('accessor.get(' + key + ') get result error');
        });
    }

    createResponse(key, file) {
        let executor = (resolve, reject)=> {
            let result = new Result(key);
            result.setFile(file);
            resolve(result);
        };

        return new Promise(executor);

    }

}