"use strict";

export class ObjectScopes {

    constructor() {
        this.priv = {}
    }

    getPriv(name) {
        return this.priv[name];
    }

    setPriv(name, value) {
        this.priv[name] = value;
    }

    createHash() {
        var time = new Date().getTime(),
            rand = (Math.random() + 1).toString(36).substring(7);
        return time + '_' + rand;
    }

}

export class ObjectBase extends ObjectScopes {
}