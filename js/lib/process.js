"use strict";
import {ObjectBase} from './object';

const ON_EXIT = Symbol();

class ProcessApi extends ObjectBase {

    constructor() {
        super();
        this.setPriv(ON_EXIT, []);
    }

    getArg(name, defaultValue = null) {
        var arg = null,
            prefix = '--' + name,
            arr = null;

        for (arg of process.argv) {
            if (!arg.startsWith(prefix)) continue;
            arr = arg.split("=", 2);

            if (arr[1]) {
                return arr[1];
            } else {
                return true;
            }
        }

        return defaultValue;
    }

    bindExit(cb) {
        this.getPriv(ON_EXIT).push(cb);
    }

    exit(cb) {
        for (var cb of this.getPriv(ON_EXIT)) {
            cb();
        }
        process.exit()
    }

    emitExit() {
        this.exit();
    }

    bindSystemSignals() {
        //do something when app is closing
        process.on('exit', processApi.emitExit);

        //catches ctrl+c event
        process.on('SIGINT', processApi.emitExit);

        //catches uncaught exceptions
        process.on('uncaughtException', processApi.emitExit);
    }

}

export var processApi = new ProcessApi();

//do something when app is closing
//process.on('exit', processApi.emitExit);

//catches ctrl+c event
//process.on('SIGINT', processApi.emitExit);

//catches uncaught exceptions
//process.on('uncaughtException', processApi.emitExit);