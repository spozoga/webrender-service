"use strict";
import {Module} from '../lib/module';
import {logger} from '../lib/logger';
import {PdfRenderer} from '../lib/pdfRenderer';
import {PngRenderer} from '../lib/pngRenderer';
import {HtmlRenderer} from '../lib/htmlRenderer';
import {Accessor} from '../lib/accessor';
import {Cache} from '../lib/cache';

export class RestModule extends Module {

    constructor() {
        super();
        this.pdfRenderer = new PdfRenderer();
        this.pngRenderer = new PngRenderer();
        this.htmlRenderer = new HtmlRenderer();
    }

    start() {
        var app = this.get('app'),
            configDecoder = this.get('config');

        for (var tokenRow of configDecoder.getProperty('tokens')) {
            this.initToken(app, tokenRow);
        }

        //log
        logger.log("rest module... started");
    }

    initToken(app, tokenRow) {
        var cache = tokenRow.cache ? new Cache('./data/' + tokenRow.token + '/', tokenRow.cacheConfig) : null,
            baseUrl = tokenRow.baseUrl || '',
            config = this.get('config'),
            self = this,
            priv = {};

        priv.cleanCache = () => {
            if (!cache) return null;
            cache.clean();
        };

        //clean cache
        app.get('/' + tokenRow.token + '/clean/:secret([a-zA-Z0-9]+)', function (req, res) {
            if (tokenRow.secret && req.params.secret == tokenRow.secret) {
                res.status(403).json({status: 'secret or token incorrect'});
                return;
            }
            priv.cleanCache();
            res.status(200).json({status: 'clean'});
        });

        //render html
        app.get('/' + tokenRow.token + '/html/:url(*)', function (req, res) {
            var url = baseUrl + req.params.url;

            if (!config.isEntitleUrl(url)) {
                res.status(403).json({status: 'no entitle url: ' + url});
                return;
            }

            if (!config.isTokenRole(tokenRow.token, 'html')) {
                res.status(403).json({status: 'token doesn\'t have html role'});
                return;
            }

            let accessor = new Accessor(self.htmlRenderer, cache);
            accessor.get(url).then((result)=> {
                res.set('Content-Type', 'text/html');
                result.send(res);
            }, ()=> {
                res.status(403).json({status: 'error'});
            });
        });

        //render pdf
        app.get('/' + tokenRow.token + '/pdf/:url(*)', function (req, res) {
            var url = baseUrl + req.params.url;

            if (!config.isEntitleUrl(url)) {
                res.status(403).json({status: 'no entitle url: ' + url});
                return;
            }

            if (!config.isTokenRole(tokenRow.token, 'pdf')) {
                res.status(403).json({status: 'token doesn\'t have pdf role'});
                return;
            }

            let accessor = new Accessor(self.pdfRenderer, cache);
            accessor.get(url).then((result)=> {
                logger.log("send", [result]);
                result.send(res);
            }, ()=> {
                res.status(403).json({status: 'error'});
            });
        });

        //render png
        app.get('/' + tokenRow.token + '/png/:url(*)', function (req, res) {
            var url = baseUrl + req.params.url;

            if (!config.isEntitleUrl(url)) {
                res.status(403).json({status: 'no entitle url: ' + url});
                return;
            }

            if (!config.isTokenRole(tokenRow.token, 'png')) {
                res.status(403).json({status: 'token doesn\'t have png role'});
                return;
            }

            let accessor = new Accessor(self.pngRenderer, cache);
            accessor.get(url).then((result)=> {
                result.send(res);
            }, ()=> {
                res.status(403).json({status: 'error'});
            });
        });
    }

}
