"use strict";
import {Module} from '../lib/module';
import {logger} from '../lib/logger';
import {Renderer} from '../lib/renderer';
import {Cache} from '../lib/cache';

export class RestModule extends Module {

    constructor() {
        super();
        this.renderer = new Renderer();
    }

    start() {
        var app = this.get('app'),
            configDecoder = this.get('config');

        app.use('/', express.static('debug'));
        logger.log("webdebug module... started");
    }

}
