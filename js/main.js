"use strict";

import {ConfigDecoder} from './lib/configDecoder';
import {RestModule} from './modules/rest';
import {logger} from './lib/logger';

export class Main {

    constructor(config) {
        this.configDecoder = new ConfigDecoder(config);
        this.app = require('express')();
        this.server = require('http').Server(this.app);
    }

    run() {
        var deps = {
            config: this.configDecoder,
            app: this.app,
            server: this.server
        };

        if (this.configDecoder.isDebug()) {
            deps.webdebug = new RestModule();
            deps.webdebug.init(deps);
            deps.webdebug.start();
        }

        deps.rest = new RestModule();
        deps.rest.init(deps);
        deps.rest.start();

        logger.log("start app");
        this.server.listen(this.configDecoder.getProperty("port", 8111));
    }

}
