"use strict";
import {Main} from './js/main';
import {processApi} from './js/lib/process';

var configPath = processApi.getArg('config', './config/config.json'),
    config = require(configPath),
    main;

config.debug = processApi.getArg('debug', config.debug);

//run
main = new Main(config);
main.run();