# webrender-service

It provice micro-service base on phantomjs to:
 - server-side page render (with javascript)
 - get page html after process by js
 - create png file from webpage
 - create pdf file from webpage

# Install
```
git clone git@bitbucket.org:spozoga/webrender-service.git
cd webrender-service
make build
make install
```

needs:
 - Bower
 - Nodejs v0.12.x (or later)
 - Npm
 - make (or you can run: bash -x bin/build.sh & bash -x bin/install.sh)
 - debian family system (only for automatic service install)

# Service
You can use system service like (only debian family):
```
sudo service webrender start/stop/debug/restart
```

or standalone (all systems unix/mac/windows):
```
babel-node webrender.js
```

# Config
```
{
  "port": 8111,
  "tokens": [
    {
      "token": "7574DI3HzLX2EekxgZ2my3fLb77690z4",
      "secret": "MuT36p687o2r16Fv3Bjx5V5Qvz4XISSF",
      "baseUrl": "http://events.pozoga.eu/",
      "cache": true,
      "cacheConfig": {
        "lifetime": 3600000,
        "refreshTime": 360000,
        "limit": 2
      },
      "roles": [
        "html",
        "pdf",
        "png"
      ]
    },
    {
      "token": "xv079Mv8cT898kPpBHc04Ff6kze01NLe",
      "baseUrl": "",
      "cache": false,
      "roles": [
        "html"
      ]
    }
  ],
  "allowUrls": [
    "http://events.pozoga.eu/",
    "http://google.pl",
    "http://google.pl/"
  ]
}
```
 - port - webrender port
 - tokens - list of tokens and token settings
 - roles - list of privileges (html - to render html, pdf - to render pdf file, png - to render png file)

# Tokens
Don't use special chars. You can generate token by http://randomkeygen.com (Recomended: CodeIgniter Encryption Keys)

# Standalone
If you want run the script without install you can use:
```
node webrender.js [optional args]
```
Arguments:
 - **--debug** - to show web debug console (default on 8091 port)

# Simple usage
Use url to get your png file:
http://your_url_or_ip:8111/:token/png/:url
http://your_url_or_ip:8111/:token/pdf/:url
http://your_url_or_ip:8111/:token/html/:url

